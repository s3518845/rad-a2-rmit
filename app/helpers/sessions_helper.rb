module SessionsHelper
  def log_in(account)
    session[:account_id] = account.id
		session[:account_username] = account.username
  end
	
  def current_account
    @current_account ||= Account.find_by(id: session[:account_id])
  end

  def logged_in?
    !current_account.nil?
  end
	
	def log_out
    session.delete(:account_id)
		session.delete(:account_username)
    @current_account = nil
  end
end
