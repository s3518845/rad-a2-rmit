class Comment < ApplicationRecord
  belongs_to :article
	before_validation :strip_blanks
	
	def strip_blanks
    self.content = self.content.strip
  end
	
	validates :content, presence: true, length: { in: 3..999 }
end
