class SessionsController < ApplicationController
 def create
    account = Account.find_by(username: params[:session][:username].downcase)
    if account && account.authenticate(params[:session][:password])
      log_in account
      redirect_to '/'
    else
      flash[:notice] = 'Invalid username and or password'
      render '/login/index'
    end
 end
	
 def destroy
    log_out
    redirect_to '/login/index'
 end
end
