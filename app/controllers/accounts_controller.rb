class AccountsController < ApplicationController
	def create
		begin
			@account = Account.new(account_params)
			@account.save
			if @account.valid?
				flash[:notice] = 'Successfully create an account'
				redirect_to '/login'
			else
				flash[:notice] = 'Invalid username or password'
				redirect_to '/login'
			end
		rescue ActiveRecord::RecordNotUnique
			flash[:notice] = 'Invalid username or password'
			redirect_to '/login'
		end
	end
	
	private
		def account_params
			params.require(:account).permit(:username, :password)
		end
end
