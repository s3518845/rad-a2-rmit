Rails.application.routes.draw do
  get 'login/index'
  get 'submit/index'
  get 'comments/index'
  get 'welcome/index'
	get '/login', to: 'login#index'
	get '/news', to: 'articles#index'
	get '/item', to: 'articles#show'
	get '/newcomments', to: 'comments#index'
	get '/submit', to: 'articles#new'
	get '/about', to: 'welcome#about'
  root 'articles#index'
	
	delete '/logout',  to: 'sessions#destroy'
	
	resources :accounts do
		resources :articles
	end
	resources :articles do
		resources :comments
	end
	resources :sessions
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	
	scope '/v0' do
		scope '/item' do
			scope '/create' do
				post '/' => 'articles#apicreate'
			end
			get '/:name' => 'articles#apishow'
		end
	end
end
