class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.text :content
      t.string :source
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end
